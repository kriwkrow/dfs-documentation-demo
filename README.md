# Documentation

## Day 01

The following is unordered list.

- Item 1
- List item 2
- Another item

Ordered list

1. First
2. Second
3. Another

[This is a link](http://google.com)

![LED Shield](led-shield.jpg)


## Day 02

Here will be content of day 02. We will look at some Adobe Illustrator today.
And do some vinyl cutting and laser cutting as well.
